const path = require("path")
const mapObj = require("map-obj")

const root = "./src"
const aliases = {
  "#root": "src/",
}

module.exports = {
  aliases,
  forBabel: {
    root: [root],
    alias: mapObj(aliases, (k, v) => [k, `./${v}`]),
  },
  forWebpack: mapObj(aliases, (k, v) => [k, path.join(__dirname, "../", v)]),
  forJest: Object.entries(aliases).reduce(
    (acc, val) => ({ ...acc, [`^${val[0]}(.*)$`]: `<rootDir>/${val[1]}$1` }),
    {}
  ),
}
