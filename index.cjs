if (process.env.NODE_ENV !== "development") {
  module.exports = require("./dist/commonjs.cjs")
} else {
  module.exports = require("./dist/commonjs.min.cjs")
}
