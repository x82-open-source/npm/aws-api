# @x82-softworks/aws-api

_An OpenAPI compatible API builder for AWS API Gateway and Lambda_

This project is intended to create a proxy lambda setup with API Gateway. It smooths over many problems with standard express hosting as it is tailored to the AWS lambda environment.

## Auto generated docs

https://x82-open-source.gitlab.io/npm/aws-api

## Example usage

```js
// src/index.js
import spec from "./spec"

export const handler = spec.lambda()
```

```js
// src/spec.js
import { STRING, create } from "@x82-softworks/aws-api"
import routes from "./routes"
import selfPkg from "../package.json"

const CORS_WHITELIST = (process.env.CORS_WHITELIST || "").split(",")
const SUPPORT_EMAIL = process.env.SUPPORT_EMAIL
const HOST = process.env.HOST
const VERSION = process.env.VERSION

const spec = create({
  info: {
    version: VERSION,
    description: selfPkg.description,
    title: HOST,
    contact: {
      name: "API Support",
      email: SUPPORT_EMAIL,
    },
  },
  host: HOST,
  basePath: VERSION ? "/" + VERSION + "/" : "/",
  security: [
    {
      apiKey: [],
    },
  ],
})
//Set the default cors
spec.cors((req, res) => {
  try {
    let origin = new URL(req.headers.origin).hostname
    if (CORS_WHITELIST.indexOf(origin) !== -1) {
      res.headers["Access-Control-Allow-Origin"] = req.headers.origin
    }
  } catch (err) {
    //ignore
  }

  res.headers["Access-Control-Allow-Methods"] =
    "GET,OPTIONS,HEAD,PUT,PATCH,DELETE,POST"
  res.headers["Access-Control-Allow-headers"] =
    "Content-Type,authorization,x-date"
})
//Now load the schemas
routes(spec)
export default spec
```

```js
// src/routes
import example from "./example"
import openapi from "./openapi"
export default (spec, services) => {
  ;[example, openapi].forEach(routeController =>
    routeController(spec, services)
  )
}
```

```js
// src/openapi
export default spec => {
  spec.get(
    "/openapi.json",
    {
      description: "Retrieves the OpenApi spec doc",
      responses: {
        200: {
          description,
          content: {
            "application/json": {
              schema: {},
            },
          },
        },
      },
      //No security needed
      security: [],
    },
    async (req, res) => {
      res.body = spec.getRoot()
    }
  )
}
```

```js
import { ARRAY, BOOLEAN, INTEGER, NUMBER, OBJECT, STRING } from '@x82-softworks/aws-api';
// src/example
export default spec => {
  spec.post(
    '/example/endpoint',
    {
      description: 'Retrieves the OpenApi spec doc',
      requestBody: {
            description:'The json input',
            content: {
            'application/json': {
                schema: {
                    properties:{
                            fizz: {
                                    type: STRING,
                                    description: 'Your fizz value'
                                },
                                bang: {
                                    type: NUMBER,
                                    description: 'Your bang amount'
                                }
                    },
                    additionalProperties: false,
                    required: required: ['fizz','bang']
                    }
                }
            }
        },
        responses:{
            200: {
            description,
            content: {
            'application/json': {
                    schema:{
                    description: 'The result',
                    type: OBJECT,
                    properties: {
                        fizz: {
                                type: STRING,
                                description: 'Your fizz value'
                            },
                            bang: {
                                type: NUMBER,
                                description: 'Your bang amount'
                            }
                    }
                }
            }
        }
      },
      //No security needed
      security: []
    },
    async (req, res) => {
      res.body = {
          fizz :req.body.fizz,
          bang: req.body.bang
      };
    }
  );
};
```
