import {
  ExtendedError,
  Method,
  Path,
  Reference,
  Request,
  Response,
  RootDefinition,
  RootDefinitionOverride
} from './types';
import {
  GENERIC_ERROR,
  convertToRegex,
  isRef,
  returnFinal,
  returnNotFound
} from './utils';
import { STRING } from './constants';
import { serializeError } from 'serialize-error';
import cloneMerge from '@x82-softworks/clone-merge';
import cookie from 'cookie';
import params from './params';
import requestBodyHandler from './requestBody';

export const FORM_ENCODED = 'application/x-www-form-urlencoded';
export const DEFAULT_CORS = {
  'Access-Control-Allow-Methods': 'GET,OPTIONS,HEAD',
  'Access-Control-Allow-headers':
    'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'
};
export * from './constants';
export { GENERIC_ERROR } from './utils';

const APP_JSON = 'application/json',
  REQUEST_BODY = 'requestBodies',
  SCHEMA = 'schemas',
  CALLBACK = 'callbacks',
  HEADER = 'headers',
  SECURITY = 'securitySchemes',
  PARAMETER = 'parameters',
  RESPONSE = 'responses',
  EXAMPLE = 'examples',
  LINK = 'links',
  OBJECT = 'object',
  CONTENT_TYPE = 'content-type';

export type Handler = (Request, Response, API) => void

const paramRegex = /\{([^}]*)\}/;
/**
 * Generates a reference object
 * @public
 * @param type
 * @param name
 * @returns A reference object
 */
function ref(type: string, name: string): Reference {
  return {
    $ref: '#/components/' + type + '/' + name
  };
}

class API {
  devMode: boolean;
  rootDef: RootDefinition;
  corsConfig: Function;
  /**
   * @internal
   */
  paths: Array<Path>;
  _suppressValidationErrors: boolean;

  constructor(opts: RootDefinitionOverride, devMode: boolean) {
    this.devMode = !!devMode;
    this.rootDef = cloneMerge(
      {
        openapi: '3.0.0',
        info: {
          version: '0.0.0',
          description: 'Default API',
          termsOfService: 'http://swagger.io/terms/',
          title: '',
          contact: {
            email: 'no-reply@example.com'
          },
          license: {
            name: 'Proprietary'
            // url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
          }
        },
        host: 'localhost',
        basePath: '/',
        paths: {}
      },
      opts
    );
    this.corsConfig = function (req, res) {
      const origin = req.headers.Origin ? req.headers.Origin : '*';
      res.headers = cloneMerge(
        res.headers,
        {
          'Access-Control-Allow-Origin': origin
        },
        DEFAULT_CORS
      );
    };
    this.paths = [];
    this.defSchema('genericError', {
      type: OBJECT,
      properties: {
        code: {
          description: 'Error code',
          type: STRING
        },
        message: {
          description: 'Human readable message',
          type: STRING
        }
      },
      required: ['code', 'message']
    });
    this.defResponse('genericError', {
      description: 'An unexpected error occurred',
      content: {
        'application/json': {
          schema: this.refSchema('genericError')
        }
      }
    });

    this._suppressValidationErrors = false;
  }
  /**
   * Sets the validation error suppression
   * @param val
   */
  suppressValidationErrors(val: boolean) {
    this._suppressValidationErrors = val;
  }
  /**
   * @public
   * @returns The Root API defintion
   */
  getRoot(): object {
    return cloneMerge(true, this.rootDef);
  }
  defRoot(def: object) {
    this.rootDef = cloneMerge(true, this.rootDef, def);
  }
  /**
   * Define the Cors function
   */
  cors(configFunc: Function) {
    this.corsConfig = configFunc;
  }
  defComponent(type, name, val): API {
    if (!this.rootDef.components) {
      this.rootDef.components = {};
    }
    if (!this.rootDef.components[type]) {
      this.rootDef.components[type] = {};
    }
    if (this.rootDef.components[type][name]) {
      throw new Error(
        'Component of type : "' +
          type +
          '" and name : "' +
          name +
          '" is already defined'
      );
    }
    this.rootDef.components[type][name] = val;
    return this;
  }
  defSecurity(name, props) {
    return this.defComponent(SECURITY, name, props);
  }
  defResponse(name, props) {
    return this.defComponent(RESPONSE, name, props);
  }
  ref(type: string, name: string): Reference {
    return ref(type, name);
  }
  defSchema(name, props) {
    if (!props.title) {
      props.title = name;
    }
    return this.defComponent(SCHEMA, name, props);
  }
  getHost(): string {
    return this.rootDef.host;
  }
  /**
   * Gets the value from the reference object
   * @public
   * @param   refObj
   * @returns The referenced object
   */
  getRef(refObj: Reference): object {
    if (!isRef(refObj)) {
      throw new Error('Not a valid reference object');
    }
    //"$ref": "#/components/schemas/Pet"
    return refObj.$ref
      .split('/')
      .slice(1)
      .reduce(function (acc, piece) {
        return acc[piece];
      }, this.rootDef);
  }

  /**
   * Ref requests
   */

  refResponse(name: string): Reference {
    return ref(RESPONSE, name);
  }
  refSchema(name: string): Reference {
    return ref(SCHEMA, name);
  }
  refRequestBody(name: string): Reference {
    return ref(REQUEST_BODY, name);
  }
  refSecurity(name: string): Reference {
    return ref(SECURITY, name);
  }
  refExample(name: string): Reference {
    return ref(EXAMPLE, name);
  }
  refParameter(name: string): Reference {
    return ref(PARAMETER, name);
  }
  refHeader(name: string): Reference {
    return ref(HEADER, name);
  }
  refCallback(name: string): Reference {
    return ref(CALLBACK, name);
  }
  refLink(name: string): Reference {
    return ref(LINK, name);
  }
  /**
   * Adds a route to the router
   * @public
   * @param   path
   * @param   method
   * @param   [def]
   * @param   handler
   */
  path(path: string, method: Method, def, handler: Handler): API {
    if (!handler) {
      handler = def;
      def = null;
    }
    def = cloneMerge(
      true,
      {
        summary: '',
        description: '',
        tags: [],
        parameters: [],
        requestBody: null,
        responses: {
          200: {
            description: 'Success' // schema: null
          },
          default: this.refResponse('genericError')
        }
      },
      def
    );
    if (!def.requestBody) {
      delete def.requestBody;
    }
    const methodString = method.toLowerCase();
    def.handler = handler;
    if (!this.rootDef.paths) {
      this.rootDef.paths = {};
    }
    if (!this.rootDef.paths[path]) {
      this.rootDef.paths[path] = {};
    }
    if (this.rootDef.paths[path][methodString]) {
      throw new Error(
        'Redefinition of path : ' + path + ' method : ' + methodString
      );
    }
    this.rootDef.paths[path][methodString] = def;
    //If it has path params
    if (paramRegex.test(path)) {
      this.paths.push({
        regex: convertToRegex(path),
        original: path
      });
    }
    return this;
  }
  /**
   * Adds a GET route
   * @public
   * @param   path
   * @param   [def]
   * @param   handler
   */
  get(path: string, def, handler: Handler): API {
    return this.path(path, Method.GET, def, handler);
  }
  /**
   * Adds a POST route
   * @public
   * @param   path
   * @param   [def]
   * @param   handler
   */
  post(path: string, def, handler: Handler): API {
    return this.path(path, Method.POST, def, handler);
  }

  /**
   * Adds a PUT route
   * @public
   * @param   path
   * @param   [def]
   * @param   handler
   */
  put(path: string, def, handler: Handler): API {
    return this.path(path, Method.PUT, def, handler);
  }

  /**
   * Adds a PATCH route
   * @public
   * @param   path
   * @param   [def]
   * @param   handler
   */
  patch(path: string, def, handler: Handler): API {
    return this.path(path, Method.PATCH, def, handler);
  }

  /**
   * Adds a DELETE route
   * @public
   * @param   path
   * @param   [def]
   * @param   handler
   */
  delete(path: string, def, handler: Handler): API {
    return this.path(path, Method.DELETE, def, handler);
  }
  defParameter(name, props) {
    return this.defComponent(PARAMETER, name, props);
  }
  defExample(name, props) {
    return this.defComponent(EXAMPLE, name, props);
  }
  defRequestBody(name, props) {
    return this.defComponent(REQUEST_BODY, name, props);
  }
  defHeader(name, props) {
    return this.defComponent(HEADER, name, props);
  }
  defLink(name, props) {
    return this.defComponent(LINK, name, props);
  }
  defCallback(name, props) {
    return this.defComponent(CALLBACK, name, props);
  }
  lambda() {
    const self = this;

    return async function (event) {
      //Automatically set the correct host
      if (event.headers && event.headers.Host) {
        let more = '';
        if (event.requestContext) {
          if (
            event.requestContext.path.indexOf(
              '/' + event.requestContext.stage
            ) === 0
          ) {
            //Then we have some kind of basepath added on
            more = '/' + event.requestContext.stage;
          }
        }
        self.defRoot({
          host: 'https://' + event.headers.Host + more
        });
      }
      return await self.dispatch(event);
    };
  }

  /**
   * @public
   * @param event
   */
  async dispatch(event: object) {
    const req: Request = {
      path: '/',
      rawPath: '/',
      httpMethod: Method.GET, //'Incoming request's method name,
      headers: {
        //Incoming request headers
      },
      rawHeaders: {},
      queryStringParameters: {
        //query string parameters
      },
      params: {
        //Will be added by the dispatcher
      },
      stageVariables: {
        //Applicable stage variables
      },
      requestContext: {
        //Request context,including authorizer - returned key - value pairs
      },
      pathParams: {},
      body: null, //A JSON string of the request payload.
      isBase64Encoded: false, // "A boolean flag to indicate if the applicable request payload is Base64-encode",
      rawBody: null,
      ...event
    };
    const self = this;
    req.rawPath = req.path;
    req.httpMethod = req.httpMethod.toLowerCase();
    //Make sure all headers are lower case as according to RFCs it doesn't matter but it does simplify lookup

    req.headers = Object.keys(req.headers || {}).reduce((acc, key) => {
      acc[key.toLowerCase()] = req.headers[key];
      return acc;
    }, {});
    //In case middleware attempts to overwrite it
    req.rawBody = req.body;
    req.rawHeaders = req.headers;
    if (req.headers?.cookie) {
      req.headers.cookie = cookie.parse(req.headers.cookie);
    }
    if (req.path[0] !== '/') {
      //Normalise
      req.path = '/' + req.path;
    }
    if (!req.httpMethod || !req.path) {
      throw new Error('Event is not wellformed');
    }

    const res: Response = {
      body: undefined,
      headers: {},
      statusCode: 200
    };

    //check this against the base path
    if (req.path.indexOf(self.rootDef.basePath) !== 0) {
      return returnNotFound(res);
    }
    self.corsConfig(req, res);
    req.path = req.path.slice(self.rootDef.basePath.length);
    if (req.path[0] !== '/') {
      req.path = '/' + req.path;
    }

    //Lookup the path directly if possible otherwise use pattern matching
    let methods = self.rootDef.paths[req.path],
      pathParams: Nullable<object> = null,
      controller;

    if (!methods) {
      //Try lookup via path matching
      for (let i = 0, c = self.paths.length; i < c; i++) {
        if ((pathParams = self.paths[i].regex.match(req.path))) {
          methods = self.rootDef.paths[self.paths[i].original];
          break;
        }
      }
    }
    req.pathParams = pathParams;
    if (!methods || !(controller = methods[req.httpMethod])) {
      //This allows added options to override the base support
      if (req.httpMethod === 'options') {
        //Automagic option support
        return res;
      }
      return returnNotFound(res);
    }
    return (
      requestBodyHandler(req, controller.requestBody, self)
        .catch(err => {
          //Assume user fuckup
          res.statusCode = 400;
          throw err;
        })
        .then(async () => {
          await params(req, controller, self);
          return new Promise((resolve, reject) => {
            try {
              resolve(controller.handler(req, res));
            } catch (err) {
              reject(err);
            }
          }).catch(err => {
            if (!res.body) {
              if (self.devMode) {
                res.body = err;
              } else {
                // eslint-disable-next-line no-console
                console.error(err);
                res.body = new Error('An internal server error has occurred');
              }
              res.statusCode = res.body.statusCode ? res.body.statusCode : 500;
            }
          });
        })
        //Errors from params and requestbody should fall through to here
        .catch(err => {
          res.body = err;
        })
        .then(() => {
          //Check if the body is an error in which case we extract only the message to prevent leaking the specifics of the server
          if (res.body instanceof Error) {
            if (self.devMode) {
              res.body = serializeError(res.body);
            } else {
              res.body = {
                code: (res.body as ExtendedError)?.code || GENERIC_ERROR,
                message: res.body.message,
                additional: self._suppressValidationErrors
                  ? null
                  : (res.body as ExtendedError)?.additional
              };
            }
            if (!res.statusCode) {
              res.statusCode = res.body.statusCode || 500;
            }
          }
          if (typeof res.body === 'object' && !res.headers[CONTENT_TYPE]) {
            //All other types must be
            res.headers[CONTENT_TYPE] = APP_JSON;
          }
          //Convert the body to a string
          return returnFinal(res);
        })
    );
  }
}

export default API;
//Auto generate all the ref functions
// [
//   SECURITY,
//   EXAMPLE,
//   PARAMETER,
//   RESPONSE,
//   HEADER,
//   CALLBACK,
//   LINK,
//   SCHEMA,
//   REQUEST_BODY
// ].forEach(type => {
//   let name = type;
//   if (name === REQUEST_BODY) {
//     //Tiny hack because request body does not have a simple pluralization
//     name = 'RequestBodys';
//   }
//   //We use the -1 to remove the pluralization
//   API.prototype['ref' + capitalize(name).slice(0, -1)] = (name) => ref(type, name);
// });
/**
 * Factory function to create an API
 * @public
 * @param init
 * @param debug
 * @returns The created api
 */
export const create = function (
  init: RootDefinitionOverride,
  debug: boolean
): API {
  return new API(init, debug);
};

/**
 * An easy shorthand to create a required JSON requestBody with provided inner schema
 * @public
 * @param             schema
 */
export const requiredJsonRequest = (schema: object): object => ({
  required: true,
  content: {
    'application/json': {
      schema: schema
    }
  }
});

/**
 * An easy shorhand to create a json response with the provided schema
 * @param schema
 * @public
 * @returns
 */
export const jsonResponseContent = (schema: object): object => ({
  'application/json': {
    schema: schema
  }
});
