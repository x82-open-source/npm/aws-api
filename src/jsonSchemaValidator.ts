import { INT32, INT64, NUMBER } from "./constants"
import Ajv from "ajv"
import isInteger from "is-integer"

const MAX_INT_32 = 2147483647
const ajv = new Ajv({
  useDefaults: true,
  coerceTypes: true,
})
ajv.addFormat(INT32, {
  validate: function (a) {
    return isInteger(a) && a <= MAX_INT_32 && a >= -MAX_INT_32
  },
  type: NUMBER,
})
ajv.addFormat(INT64, {
  validate: function (a) {
    return isInteger(a)
  },
  type: NUMBER,
})
/**
 * Checks to see if the value matches to the given schema
 * @method exports
 * @param   schema
 * @param  val
 * @internal
 * @throws {Error}
 */
export default (schema: object, val: any) => {
  let validate = ajv.compile(schema),
    err
  if (!validate(val)) {
    err = new Error("Validation error")
    err.additional = validate.errors
    throw err
  }
}
