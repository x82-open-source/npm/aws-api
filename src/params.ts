import { coerce, isRef } from './utils';
import isNotValid from './jsonSchemaValidator';
export default async (req, controller, api) => {
   (controller.parameters || []).forEach(param => {
    let val;
    switch (param.in) {
      case 'query':
        val = (req.queryStringParameters || {})[param.name];
        break;
      case 'header':
        val = req.headers[param.name];
        break;
      case 'path':
        val = decodeURIComponent(req.pathParams[param.name]);
        break;
      case 'cookie':
        val = req.headers.cookie[param.name];
        break;
      default:
        throw new Error('Unknown param type : ' + param.in);
    }
    if (val === undefined) {
      if (param.required) {
        throw new Error(`Required parameter "${param.name}" not found`);
      }
    } else if (param.schema) {
      let schema = param.schema;
      if (isRef(schema)) {
        schema = api.getRef(schema);
      }
      val = coerce(schema, val);
      isNotValid(schema, val);
    }
    req.params[param.name] = val;
  });
};
