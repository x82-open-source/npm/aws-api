/* eslint-disable no-case-declarations */
import { coerce, isRef } from "./utils"
import { parse } from "@x82-softworks/aws-api-multipart"
import isNotValid from "./jsonSchemaValidator"
import qs from "query-string"

function validateSchema(req, schema, api) {
  if (!schema) {
    return
  }
  if (isRef(schema)) {
    schema = api.getRef(schema)
  }
  const val = (req.body = coerce(schema, req.body))
  isNotValid(schema, val)
}
/**
 * Handles the request body portion of a request
 * @param {Object} req
 * @param  {?Object} controllerRequestBody
 * @param {Object} api
 */
export default async function (req, controllerRequestBody, api) {
  let contentSelector,
    hasNoBody = req.body === null || req.body === undefined,
    schema,
    contentType
  if (!controllerRequestBody) {
    return
  }
  //Check if its a request body reference
  if (isRef(controllerRequestBody)) {
    controllerRequestBody = api.getRef(controllerRequestBody)
  }
  //Check to see if required
  if (controllerRequestBody.required && hasNoBody) {
    throw new Error("Request body must be supplied")
  }
  //If there is no body to parse and its not required then we're done
  if (hasNoBody) {
    return
  }
  contentType = req.headers["content-type"]
  if (!contentType) {
    return
    // throw new Error('Request headers do not contain "content-type"');
  }
  //In the case of multipart/form-data having a boundary
  contentType = contentType.split(";")[0]
  //Check to see if that controller accepts the content type
  if (!(contentSelector = controllerRequestBody.content[contentType])) {
    throw new Error(`Content Type "${contentType}" is not supported`)
  }
  schema = contentSelector.schema
  switch (contentType) {
    case "application/json":
      try {
        req.body = JSON.parse(req.body)
      } catch (err) {
        throw new Error("Cannot parse request body")
      }
      break
    case "application/x-www-form-urlencoded":
      req.body = qs.parse(req.body)
      break
    case "multipart/form-data":
      const ret = await parse(req)
      const { val, multiparts } = ret.files.reduce(
        (acc, file) => {
          acc.val[file.fieldName] = file.data.toString("utf-8")
          acc.multiparts[file.fieldName] = file
          return acc
        },
        {
          val: {},
          multiparts: {},
        }
      )

      //The coercion from the validate schea should handle converting application json,etc for us
      req.body = val
      req.multiparts = multiparts
      break
    //Passthrough
    default:
      break
  }
  validateSchema(req, schema, api)
}
