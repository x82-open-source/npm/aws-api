import { URLPattern } from './urlPattern';

export interface Response {
  body: any
  headers: object
  statusCode: number
}

export type Path = {
  regex: URLPattern
  original: string
}

export interface Headers {
  cookie?: string
}

export interface Request {
  path: string
  rawPath: string
  /**
   * The lowercase version of the http method
   */
  httpMethod: string

  pathParams: Nullable<object>
  headers: Headers
  rawHeaders: Headers
  queryStringParameters: object 
  params: object
  stageVariables: object
  requestContext: object
  body: any
  rawBody: any
  isBase64Encoded: boolean
}

export type ExtendedError = Error & {
  additional: any
  code: any
}

export type Reference = {
  $ref: string
}
export enum Method {
  GET = 'get',
  POST = 'post',
  PUT = 'put',
  DELETE = 'delete',
  PATCH = 'patch',
  OPTIONS = 'options',
}

export interface RootDefinitionInfo {
  version: string
  description: string
  termsOfService: string
  title: string
  contact: object
  license: object
}

export interface RootDefinition {
  components: Nullable<object>
  openapi: string
  paths: object
  info: RootDefinitionInfo
  host: string
  basePath: string
}

export interface RootDefinitionOverride {
  components?: RootDefinition['components']
  openapi?: RootDefinition['openapi']
  paths?: RootDefinition['paths']
  info?: RootDefinition['info']
  host?: RootDefinition['host']
  basePath?: RootDefinition['basePath']
}
