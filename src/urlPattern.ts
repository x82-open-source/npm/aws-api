const keyPattern = /({[^}]*})/g

export class URLPattern {
  regex: RegExp
  keys: Array<string>
  constructor(pattern: string) {
    const keys: Array<string> = []
    let key

    while ((key = keyPattern.exec(pattern))) {
      key = key[0].slice(1, -1)
      keys.push(key)
    }
    //We need the inorder value of the keys
    this.keys = keys
    pattern = pattern.replace(/({[^}]*})/g, "([a-zA-Z0-9-\\._~ %]+)")
    this.regex = new RegExp(pattern)
  }

  match(test: string): Nullable<object> {
    let ret: Nullable<object> = null,
      match
    if ((match = this.regex.exec(test))) {
      ret = {}
      for (let i = 0; i < this.keys.length; i++) {
        ret[this.keys[i]] = match[i + 1]
      }
      //We need to check for the param keys and return it as an object
    }
    return ret
  }
}
