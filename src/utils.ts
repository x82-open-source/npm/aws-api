import { BOOLEAN, INTEGER, NUMBER, OBJECT } from "./constants"
import { Response } from "./types"
import { URLPattern } from "./urlPattern"

/**
 * The default generic error code
 * @public
 */
export const GENERIC_ERROR = "GenericError"

export const isError = (err): boolean => {
  return typeof err === "object" && err.name && err.stack && err.message
}
/**
 * Performs the final toStringing of data  in the res body
 * @internal
 * @param res - The response
 * @returns The same response with the stringified version of its body
 */
export const returnFinal = (res: Response): Response => {
  if (res.body && typeof res.body !== "string") {
    res.body = JSON.stringify(res.body)
  }
  return res
}

/**
 * Checks to see if the object contains a reference to another in the api doc
 * @param obj
 */
export const isRef = (obj: any): boolean => {
  return typeof obj === "object" && typeof obj?.$ref === "string"
}

export const coerce = (schema, val) => {
  switch (schema.type) {
    case OBJECT:
      if (typeof val === "string") {
        val = JSON.parse(val)
      }
      break
    case INTEGER:
    case NUMBER:
      val = Number(val)
      break
    case BOOLEAN:
      val = Boolean(val)
      break
    default:
      break
  }
  return val
}

export const returnNotFound = async function (res: Response) {
  return returnFinal({
    ...res,
    statusCode: 404,
  })
}

/**
 * Convert a string like /{test}/{param} to a url pattern matching object
 * @internal
 * @param        path
 * @returns A URLPattern matcher
 */
export const convertToRegex = (path: string): URLPattern => {
  return new URLPattern(path)
}

export const returnInvalid = async function (res: Response, reason) {
  if (typeof reason === "string") {
    reason = {
      code: GENERIC_ERROR,
      message: reason,
    }
  }
  if (isError(reason)) {
    reason = {
      code: reason.code || GENERIC_ERROR,
      message: reason.message,
    }
  }
  return returnFinal({
    ...res,
    statusCode: 400,
    body: reason,
  })
}
