const { pathsToModuleNameMapper } = require("ts-jest")
const { compilerOptions } = require("../tsconfig.json")

module.exports = {
  globals: {
    "ts-jest": {
      tsconfig: "<rootDir>/tsconfig.json",
    },
  },
  collectCoverage: true,
  reporters: [
    "default",
    ["jest-junit", { outputDirectory: "<rootDir>/coverage" }],
  ],
  coverageReporters: ["text-summary", "cobertura"],
  coverageDirectory: "<rootDir>/coverage",
  moduleFileExtensions: ["ts", "js", "tsx", "jsx"],
  moduleNameMapper: pathsToModuleNameMapper(compilerOptions.paths, {
    prefix: "<rootDir>/",
  }),
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
  testEnvironment: "node",
  roots: ["<rootDir>"],
  rootDir: "../",
}
