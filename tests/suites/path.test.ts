import { create } from "#/index"

describe("Path", () => {
  it("should default path params correctly", async () => {
    const spec = create({}, true)
    const description = "Retrieves the OpenApi spec doc"
    spec.get(
      "/openapi.json",
      {
        description,
      },
      () => {}
    )

    expect(spec.rootDef.paths["/openapi.json"].get).toMatchObject({
      summary: "",
      description,
      tags: [],
      parameters: [],
      responses: {
        200: {
          description: "Success", // schema: null
        },
        default: spec.refResponse("genericError"),
      },
    })
  })
})
