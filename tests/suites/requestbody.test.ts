import { STRING, create, requiredJsonRequest } from "#/index"
import requestBody from "#/requestBody"

describe("Request Body", () => {
  it("should ref schemas correctly", async () => {
    //Should throw validation error for invalid req body
    const spec = create({}, true)

    spec.defSchema("nestObj", {
      type: "object",
      properties: {
        bar: {
          type: STRING,
        },
      },
      additionalProperties: false,
      required: ["foo"],
    })

    const req = {
      headers: {
        "content-type": "application/json",
      },
      //Assumes stringified body
      body: JSON.stringify({
        foo: "a",
      }),
    }

    await expect(async () => {
      await requestBody(
        req,
        requiredJsonRequest(spec.refSchema("nestObj")),
        spec
      )
    }).rejects.toMatchObject(new Error("Validation error"))
  })
  it("should ref requestBodies correctly", async () => {
    //Should throw validation error for invalid req body
    const spec = create({}, true)

    spec.defSchema("nestObj", {
      type: "object",
      properties: {
        bar: {
          type: STRING,
        },
      },
      additionalProperties: false,
      required: ["foo"],
    })
    spec.defRequestBody(
      "reqBody",
      requiredJsonRequest(spec.refSchema("nestObj"))
    )

    const req = {
      headers: {
        "content-type": "application/json",
      },
      //Assumes stringified body
      body: JSON.stringify({
        foo: "a",
      }),
    }

    await expect(async () => {
      await requestBody(req, spec.refRequestBody("reqBody"), spec)
    }).rejects.toMatchObject(new Error("Validation error"))
  })
})
