import { OBJECT, STRING } from "#/index"
import validate from "#/jsonSchemaValidator"

describe("Json schema validator", () => {
  it("validates minLength correctly", () => {
    try {
      validate(
        {
          additionalProperties: false,
          required: ["bar"],
          type: OBJECT,
          properties: {
            bar: {
              type: STRING,
              minLength: 1,
            },
          },
        },
        {
          bar: "",
        }
      )
      expect(true).toBe(false)
    } catch (err) {
      expect(err.message).toEqual("Validation error")
      expect(err.additional).toEqual([
        {
          keyword: "minLength",
          instancePath: "/bar",
          schemaPath: "#/properties/bar/minLength",
          params: {
            limit: 1,
          },
          message: "must NOT have fewer than 1 characters",
        },
      ])
    }
  })
  it("validates maxLength correctly", () => {
    try {
      validate(
        {
          additionalProperties: false,
          required: ["bar"],
          type: OBJECT,
          properties: {
            bar: {
              type: STRING,
              maxLength: 5,
            },
          },
        },
        {
          bar: "1234567",
        }
      )
      expect(true).toBe(false)
    } catch (err) {
      expect(err.message).toEqual("Validation error")
      expect(err.additional).toEqual([
        {
          keyword: "maxLength",
          instancePath: "/bar",
          schemaPath: "#/properties/bar/maxLength",
          params: {
            limit: 5,
          },
          message: "must NOT have more than 5 characters",
        },
      ])
    }
  })
})
