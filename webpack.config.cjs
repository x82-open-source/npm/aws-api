/* eslint-disable @typescript-eslint/no-var-requires */
const path = require("path")
const TerserPlugin = require("terser-webpack-plugin")
const pkg = require("./package.json")
const TsconfigPathsPlugin = require("tsconfig-paths-webpack-plugin")
const nodeExternals = require("webpack-node-externals")

const commonjs = {
  entry: {
    commonjs: "./src/index.ts",
    "commonjs.min": "./src/index.ts",
  },
  externalsPresets: { node: true }, // in order to ignore built-in modules like path, fs, etc.
  externals: [nodeExternals()], // in order to ignore all modules in node_modules folder
  mode: "production",
  output: {
    clean: true,
    path: path.resolve(__dirname, "dist"),
    filename: "[name].cjs",
    library: {
      type: "commonjs2",
    },
  },
  resolve: {
    plugins: [
      new TsconfigPathsPlugin({
        /* options: see below */
      }),
    ],
    extensions: [".ts", ".tsx", ".js"],
  },
  devtool: "source-map",
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        test: /\.min.c?js(\?.*)?$/i,
      }),
    ],
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: "ts-loader",
        options: {
          onlyCompileBundledFiles: true,
          //Do not use rel paths here
          //https://github.com/TypeStrong/ts-loader/issues/405
          configFile: "tsconfig.prod.json",
        },
      },
    ],
  },
}

const moduleExports = Object.assign({}, commonjs, {
  entry: {
    esm: "./src/index.ts",
    "esm.min": "./src/index.ts",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "[name].js",
    library: {
      type: "module",
      umdNamedDefine: true,
    },
  },
  experiments: {
    outputModule: true,
  },
})

module.exports = [commonjs, moduleExports]
